import React from 'react';
import ReactDOM from 'react-dom';
import Router from './router';
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import Theme from './components/godlike/css/godlike.scss';
import htaccess from './.htaccess';

ReactDOM.render(Router, document.getElementById('root'));