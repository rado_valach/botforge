import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

// Components
// import PageBorder from './components/PageBorder/index.jsx';
import Header from './components/Header/index.jsx';
import Footer from './components/Footer/index.jsx';
// import BackgroundVideo from './components/BackgroundVideo/index.jsx';

// Pages
import Home from './scenes/home/index.jsx';

export default (
  
  <Router>
    <div>
      {/*
      <PageBorder />
      <BackgroundVideo />
      */}
      <Route exact path="/" component={Home} />
      <Footer />
    </div>
  </Router>

);
