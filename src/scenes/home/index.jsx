import React from 'react';
import { Link } from 'react-router-dom';

import headerImage from './images/gw2golems.jpg'; 
import css from './styles.scss';

import Bot from '../../components/Bot/index.jsx';
import BotForm from '../../components/BotForm/index.jsx';
import Channels from '../../components/Channels/index.jsx';

let apiUrl = 'https://botforge-dev.herokuapp.com'; // dev

export default class Home extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			channels: [],
			bots: []
		}

		// Bots
		this._addNewBot = this._addNewBot.bind(this);
		this._deleteBot = this._deleteBot.bind(this);
		this._loadBots = this._loadBots.bind(this);

		// Channels
		this._addNewChannel = this._addNewChannel.bind(this);
		this._deleteChannel = this._deleteChannel.bind(this);
		this._loadChannels = this._loadChannels.bind(this);
		this._switchChannelUrl = this._switchChannelUrl.bind(this);
	}

	componentDidMount() {
		this._loadBots();
		this._loadChannels();
	}


	// =================
	// 	Bots
	// =================

	_loadBots(){
		let that = this;

		// load bots from database
		fetch(apiUrl + '/bots', {
			method: 'get'
		}).then(function(response) {
			return response.json();
		}).then(function(bots) {
			// update page with new bots
			that.setState({
				bots: bots
			})
		}).catch(function(err) {
			alert(err);
		});
	}

	_renderBots(){
		return this.state.bots.map((bot) => {
			return (
				<Bot
					id={bot._id} 
					name={bot.name}
					avatarUrl={bot.avatarUrl}
					channels={this.state.channels}
					key={bot._id}
					deleteBot={this._deleteBot}
					switchChannelUrl = {this._switchChannelUrl}
				/>
			);
		});
	}

	_addNewBot(newBot){
		
		let that = this;
		
		// encode bot object
		const request = Object.keys(newBot).map((key) => {
		  return encodeURIComponent(key) + '=' + encodeURIComponent(newBot[key]);
		}).join('&');
		
		fetch(apiUrl + '/bots', {
			method: 'post',
			headers: {
			    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
			},
			body: request
		}).then(function(response) {
			that._loadBots();
		}).catch(function(err) {
			alert(err);
		});
		
    }

    _deleteBot(id){
    	
    	let that = this;

    	fetch(apiUrl + '/bots/'+id, {
			method: 'delete'
		}).then(function(response) {
			that._loadBots();
		}).catch(function(err) {
			alert(err);
		});
    }

    // =================
	// 	Channels
	// =================

	_loadChannels(){
		let that = this;

		// load bots from database
		fetch(apiUrl + '/channels', {
			method: 'get'
		}).then(function(response) {
			return response.json();
		}).then(function(channels) {
			// update page with new bots
			that.setState({
				channels: channels
			})
		}).catch(function(err) {
			alert(err);
		});
	}

	_addNewChannel(newChannel){
		
		let that = this;
		
		// encode channel object
		const request = Object.keys(newChannel).map((key) => {
		  return encodeURIComponent(key) + '=' + encodeURIComponent(newChannel[key]);
		}).join('&');
		
		fetch(apiUrl + '/channels', {
			method: 'post',
			headers: {
			    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
			},
			body: request
		}).then(function(response) {
			that._loadChannels();
		}).catch(function(err) {
			alert(err);
		});
		
    }

    _deleteChannel(id){
    	
    	let that = this;

    	fetch(apiUrl + '/channels/'+id, {
			method: 'delete'
		}).then(function(response) {
			that._loadChannels();
		}).catch(function(err) {
			alert(err);
		});
	}
	
	_switchChannelUrl(id){
		let channels = this.state.channels;
		let channel = channels.find(x => x._id === id);
		if (channel.switchUrl == null || channel.switchUrl === false){
			channel.switchUrl = true;
		} else {
			channel.switchUrl = false;
		}
		this.setState({
			channels: channels
		})
	}

	render() {
		const bots = this._renderBots();
		
		return (
			<div className="bot-forge">
				<Channels
					channels={this.state.channels}
					addNewChannel = {this._addNewChannel}
					deleteChannel = {this._deleteChannel}
				/>
				<div className="container">
					<div className="text-right text-main-1">
						<h3 className="nk-title-back">What is even this</h3>
						<h4 className="nk-sub-title text-main-1">golemancy went too far...</h4>
						<h2 className="nk-title display-4">Discord Bot Forge 3.0</h2>
						<div className="nk-title-sep-icon">
							<span className="icon">
								<span className="ion-chatbubbles"></span>
							</span>
						</div>
					</div>
				</div>
				

				<div className="container">
					<div className="nk-gap-4"></div>
		        	<div className="row vertical-gap lg-gap">
		        		
		        		{bots}
		        			
		        	</div>
		        	<div className="nk-gap-6"></div>
		        </div>

		        <div className="nk-box bg-dark-1"> 
              		<div className="container">
              			<div className="row">
              				<div className="col-md-6 col-md-offset-3">

              					<BotForm
              						addNewBot = {this._addNewBot}
              					/>
              				
              				</div>
              			</div>
              		</div>
	                <div className="nk-gap-2"></div> 
	                <div className="nk-gap-6"></div> 
              	</div>
		    </div>
		);
	}
}