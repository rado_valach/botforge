import React, { Component } from 'react';

import ChannelForm from './components/ChannelForm/index.jsx';
import Channel from './components/Channel/index.jsx';

import bgNavSideImage from './images/bg-nav-side.jpg';
import css from './styles.scss';

class NavbarRight extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
			open: false
		};

		this._toggleNavbar = this._toggleNavbar.bind(this);
    }

    _toggleNavbar(){
    	const currentState = this.state.open;
        this.setState({ open: !currentState });
    }

    _renderChannels(){
		return this.props.channels.map((channel) => {
			return (
				<Channel
					id={channel._id} 
					name={channel.name}
					webhookUrl_1={channel.webhookUrl_1}
					webhookUrl_2={channel.webhookUrl_2}
					key={channel._id}
					deleteChannel={this.props.deleteChannel}
				/>
			);
		});
	}

    render() {
    	const channels = this._renderChannels();

        return (
        	<div>
	            <nav data-open={this.state.open} className="nk-navbar nk-navbar-side nk-navbar-right-side nk-navbar-lg nk-navbar-align-center nk-navbar-overlay-content" id="nk-side">

			        <div className="nk-navbar-bg">
			            <div className="bg-image" ></div>
			        </div>


			        <div className="nano">
			            <div className="nano-content">
			                <div className="nk-nav-table">

			                    <div className="nk-nav-row">
			                    	<h3>Discord channels</h3>
			                    </div>

			                    <div className="nk-nav-row nk-nav-row-full nk-nav-row-center">
			                        <ul>
			                        	{channels}
			                        </ul>
			                    </div>

			                    <div className="nk-nav-row">
			                        <div className="nk-nav-footer">
			                        	<ChannelForm
				                        	addNewChannel = {this.props.addNewChannel}
				                        />
				                        <div className="nk-gap-2"></div>
			                            <p>Here you can add/remove Discord channels where you want to use bots at. You need webhook url that you can create in channel/server settings in your discord server. For more info check <a href="https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks" target="_blank" className="text-main-1">this</a>.</p>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <a href="#" className="navbar__toggle" onClick={this._toggleNavbar}>Manage channels <span className="ion-gear-b"></span></a>
			        
			    </nav>

			    <div data-open={this.state.open} className="nk-navbar-overlay" onClick={this._toggleNavbar}></div>
			</div>
        );
    }
}

export default NavbarRight;
