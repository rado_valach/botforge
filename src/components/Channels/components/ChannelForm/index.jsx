import React, { Component } from 'react';

class ChannelForm extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
			name: '',
			webhookUrl_1: '',
			webhookUrl_2: ''
		};

		this._createChannel = this._createChannel.bind(this);
        this._handleNameChange = this._handleNameChange.bind(this);
		this._handleWebhookUrl1Change = this._handleWebhookUrl1Change.bind(this);
		this._handleWebhookUrl2Change = this._handleWebhookUrl2Change.bind(this);
    }

    _handleNameChange(event){
		 this.setState({name: event.target.value});
	}

	_handleWebhookUrl1Change(event){
		 this.setState({webhookUrl_1: event.target.value});
	}

	_handleWebhookUrl2Change(event){
		this.setState({webhookUrl_2: event.target.value});
   }

	_createChannel(e){
		e.preventDefault();

		if (this.state.name == "") {
			alert("Please give it a name so you know what channel this webhook posts in. Doesn't have to be same name as on Discord.");
			return;
		}

		if (this.state.webhookUrl_1 == "" || this.state.webhookUrl_2 == "") {
			alert("You have to insert 2 webhook URLs for the same channel. This serves to prevent mobile app bug.");
			return;
		}

		let newChannel = {
			"name": this.state.name,
			"webhookUrl_1": this.state.webhookUrl_1,
			"webhookUrl_2": this.state.webhookUrl_2
		};
		
		this.props.addNewChannel(newChannel);
    	
    	this.setState({
    		name: '',
			webhookUrl_1: '',
			webhookUrl_2: ''
    	});
    }

    render() {
        return (
            <form className="nk-form nk-form-ajax nk-form-style-1">
        		<label htmlFor="channel-name">Channel name</label>
				<input type="text" className="form-control required" name="name" placeholder="ERP" id="channel-name" aria-required="true" value={this.state.name} onChange={this._handleNameChange}/>
				<div className="nk-gap"></div>
				<label htmlFor="webhook-url-1">Webhook URL 1</label>
				<input type="text" className="form-control required" name="url1" placeholder="https://discordapp.com/api/webhooks/ ... " id="webhook-url-1" aria-required="true" value={this.state.webhookUrl_1} onChange={this._handleWebhookUrl1Change}/>
				<div className="nk-gap"></div>
				<label htmlFor="webhook-url-2">Webhook URL 2</label>
				<input type="text" className="form-control required" name="url2" placeholder="https://discordapp.com/api/webhooks/ ... " id="webhook-url-2" aria-required="true" value={this.state.webhookUrl_2} onChange={this._handleWebhookUrl2Change}/>
				<div className="nk-gap"></div>
				<div className="text-center">
					<button className="nk-btn nk-btn-lg" type="submit" onClick={this._createChannel}>Add new channel</button>
				</div>
			</form>
        );
    }
}

export default ChannelForm;
