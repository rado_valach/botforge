import React, { Component } from 'react';

class Channel extends Component {

    constructor(props) {
        super(props);

        this._handleDelete = this._handleDelete.bind(this);
    }

    _handleDelete(e){
		e.preventDefault();
		if (confirm("Are you sure you want to remove "+this.props.name+" channel?")) {
			this.props.deleteChannel(this.props.id);
		}
	}

    render() {
        return (
            <li>{this.props.name} <a href="#" className="nk-btn nk-btn-xs nk-btn-rounded nk-btn-outline nk-btn-color-main-1" onClick={this._handleDelete}>Remove</a></li>
        );
    }
}

export default Channel;
