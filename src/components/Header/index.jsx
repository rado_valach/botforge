import React, { Component, PropTypes } from 'react';
import ContactsTop from './components/ContactsTop/index.jsx';
import Navbar from './components/Navbar/index.jsx';

class Header extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header className="nk-header nk-header-opaque">
                
                <Navbar />
            </header>
        );
    }
}

export default Header;