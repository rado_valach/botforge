import React, { Component, PropTypes } from 'react';
import { NavLink } from 'react-router-dom';
import Styles from './styles.scss';

class Navbar extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav className="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-transparent nk-navbar-autohide">
                    <div className="container">
                        <div className="nk-nav-table">

                            <a href="index.html" className="nk-nav-logo">
                                <h5>GW2 raids</h5>
                            </a>


                            <ul className="nk-nav nk-nav-right hidden-md-down" data-nav-mobile="#nk-nav-mobile">
                                <li>
                                    <NavLink exact to="/" activeClassName="active">Home</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/how-to-start" activeClassName="active">How to start</NavLink>
                                </li>
                                <li className="nk-drop-item">
                                    <NavLink to="/raids" activeClassName="active">Raids</NavLink>
                                    <ul className="dropdown">
                                        <li className="  nk-drop-item">
                                            <a href="bastion-of-the-penitent">Bastion of the Penitent</a>
                                            <ul className="dropdown">
                                                <li className="">
                                                    <NavLink to="/cairn" activeClassName="active">Cairn the Indomitable</NavLink>
                                                </li>
                                                <li className="">
                                                    <NavLink to="/mursaat" activeClassName="active">Mursaat Overseer</NavLink>
                                                </li>
                                                <li className="">
                                                    <NavLink to="/samarog" activeClassName="active">Samarog</NavLink>
                                                </li>
                                                <li className="">
                                                    <NavLink to="/deimos" activeClassName="active">Deimos</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <NavLink to="/builds" activeClassName="active">Builds</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/contact" activeClassName="active">Contact</NavLink>
                                </li>
                                
                            </ul>

                            <ul className="nk-nav nk-nav-right nk-nav-icons">
                                <li className="single-icon">
                                    <a href="#" className="no-link-effect" data-nav-toggle="#nk-side">
                                        <span className="nk-icon-burger">
                                            <span className="nk-t-1"></span>
                                            <span className="nk-t-2"></span>
                                            <span className="nk-t-3"></span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                </nav>
        );
    }
}

export default Navbar;
