import React, { Component, PropTypes } from 'react';

class ContactsTop extends Component {


    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="nk-contacts-top">
	            <div className="container">
	                <div className="nk-contacts-left">
	                    <div className="nk-navbar">
	                        <ul className="nk-nav">
	                            <li className="nk-drop-item">
	                                <a href="#">Russia</a>
	                                <ul className="dropdown">
	                                    <li><a href="#">Russia</a></li>
	                                    <li><a href="#">USA</a></li>
	                                    <li><a href="#">United Kingdom</a></li>
	                                    <li><a href="#">France</a></li>
	                                    <li><a href="#">Spain</a></li>
	                                    <li><a href="#">Germany</a></li>
	                                </ul>
	                            </li>
	                            <li><a href="#">Privacy</a></li>
	                            <li><a href="page-contact.html">Contact</a></li>
	                        </ul>
	                    </div>
	                </div>
	                <div className="nk-contacts-right">
	                    <div className="nk-navbar">
	                        <ul className="nk-nav">
	                            <li>
	                                <a href="https://twitter.com/nkdevv" target="_blank">
	                                    <span className="ion-social-twitter"></span>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="https://dribbble.com/_nK" target="_blank">
	                                    <span className="ion-social-dribbble-outline"></span>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#">
	                                    <span className="ion-social-instagram-outline"></span>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#">
	                                    <span className="ion-social-pinterest"></span>
	                                </a>
	                            </li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	        </div>
        );
    }
}

export default ContactsTop;
