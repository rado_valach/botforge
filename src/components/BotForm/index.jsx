import React, { Component } from 'react';

class BotForm extends Component {
    
    constructor(props) {
        super(props);

		this.state = {
			name: '',
			avatarUrl: ''
		};

        this._createBot = this._createBot.bind(this);
        this._handleNameChange = this._handleNameChange.bind(this);
        this._handleAvatarUrlChange = this._handleAvatarUrlChange.bind(this);
    }

	_handleNameChange(event){
		 this.setState({name: event.target.value});
	}

	_handleAvatarUrlChange(event){
		 this.setState({avatarUrl: event.target.value});
	}

	_createBot(e){
		e.preventDefault();

		if (this.state.name == "") {
			alert("You have to give it a name!");
			return;
		}

		if (this.state.avatarUrl == "") {
			alert("Add some nice picture please...");
			return;
		}

		let newBot = {
			"name": this.state.name,
			"avatarUrl": this.state.avatarUrl
		};
		
		this.props.addNewBot(newBot);
    	
    	this.setState({
    		name: '',
    		avatarUrl: ''
    	});
    }

    render() {
        return (
            <div className="bot-form">
	            <div className="nk-gap-4"></div>
	            <div className="text-center">
	            	<h2 className="nk-title h3">Forge new bot</h2>
	            	<h4 className="nk-sub-title">And think of what you've done ...</h4>
	            </div>
            	<div className="nk-gap-2"></div>
            	<form className="nk-form nk-form-ajax nk-form-style-1">
            		<label htmlFor="bots-name">Name</label>
					<input type="text" className="form-control required" name="name" placeholder="Bob" id="bots-name" aria-required="true" value={this.state.name} onChange={this._handleNameChange}/>
					<div className="nk-gap"></div>
					<label htmlFor="avatar-url">Avatar URL</label>
					<input type="text" className="form-control required" name="avatarUrl" placeholder="http:// ...  (upload image to imgur for example and paste it's file url here)" id="avatar-url" aria-required="true" value={this.state.avatarUrl} onChange={this._handleAvatarUrlChange}/>
					<div className="nk-gap"></div>
					<div className="text-center">
						<button className="nk-btn nk-btn-lg" type="submit" onClick={this._createBot}>Create</button>
					</div>
				</form>
            </div>
        );
    }
}

export default BotForm;
