import React, { Component } from 'react';

import css from './styles.scss';

class Bot extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
	      message: '',
	      channel: ''
	    };

        this._createWebhookMsg = this._createWebhookMsg.bind(this);
        this._handleMessageChange = this._handleMessageChange.bind(this);
        this._handleChannelChange = this._handleChannelChange.bind(this);
        this._handleDelete = this._handleDelete.bind(this);
    }

	_handleMessageChange(event){
		this.setState({message: event.target.value});
	}

	_handleChannelChange(event){
		// get channel object by ID
		let channel = this.props.channels.find(x => x._id === event.target.value);
		this.setState({channel: channel});
	}

	_createWebhookMsg(e){

		e.preventDefault();

		if (this.state.channel == "") {
			alert("You have to select Discord channel where you want to post this!");
			return;
		}

		if (this.state.message == "") {
			alert("C'mon... You have to write something!");
			return;
		}

		let jsonmsg = {
			"username": this.props.name,
			"avatar_url": this.props.avatarUrl,
			"content": this.state.message
		}

		// rotate webhook URL for each call
		let url;
		console.log(this.state.channel.switchUrl);
		if (this.state.channel.switchUrl) {
			url = this.state.channel.webhookUrl_1;
		} else {
			url = this.state.channel.webhookUrl_2;
		}

		this.props.switchChannelUrl(this.state.channel._id);


		//let url = this.state.channel;
		let xhr = new XMLHttpRequest();
		
		xhr.open("POST", url, true);
		xhr.setRequestHeader("Content-type", "application/json");
		let data = JSON.stringify(jsonmsg);
		xhr.send(data);
		xhr.onreadystatechange = function() {
			if(this.status != 200){
			//	alert(this.responseText);
			}
		}
		
		
		this.setState({message: ""});
		
	}

	_handleDelete(e){
		e.preventDefault();
		if (confirm("Are you sure you want to kill "+this.props.name+"?")) {
			this.props.deleteBot(this.props.id);
		}
	}

    render() {
        return (
        	<div className="col-md-6">
				<blockquote className="nk-testimonial-2 bot-block">
					<div className="nk-testimonial-photo" style={{backgroundImage: 'url(' + this.props.avatarUrl + ')'}}></div>
					<div className="nk-testimonial-name h4">{this.props.name}</div>
					<form className="nk-form nk-form-ajax nk-form-style-1">
						<div className="nk-testimonial-body">
							<div className="row">
								<div className="col-md-12">
									<textarea className="form-control" rows="5" placeholder="Type your message here ..." value={this.state.message} onChange={this._handleMessageChange}></textarea>
								</div>
							</div>
							<div className="nk-gap-1"></div>
							<div className="row">
								<div className="col-md-6">
									<select name="channel" className="form-control required" aria-required="true" value={this.state.channel._id} onChange={this._handleChannelChange}>
										<option value="">Select discord channel...</option>
										{this.props.channels.map(channel => <option value={channel._id} key={channel._id}>{channel.name}</option>)}
									</select>
								</div>
								<div className="col-md-6">
									<button className="nk-btn nk-btn-lg" type="submit" onClick={this._createWebhookMsg}>Send message</button>
								</div>
							</div>
						</div>
					</form>
					<button className="nk-btn nk-btn-xs nk-btn-circle nk-btn-outline nk-btn-color-main-1 bot__delete" onClick={this._handleDelete}>X</button>
				</blockquote>
			</div>
        );
    }
}

export default Bot;
