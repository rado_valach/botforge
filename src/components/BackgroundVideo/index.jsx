import React, { Component, PropTypes } from 'react';

import css from './styles.scss'; 

import mp4 from './assets/bg-2.mp4';
import webm from './assets/bg-2.webm';
import ogv from './assets/bg-2.ogv';
import image from './assets/bg-2.jpg';

class BackgroundVideo extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
        	<video className="nk-page-background op-5" loop autoPlay poster={image}>
                <source src={mp4} type="video/mp4" />
                <source src={webm} type="video/webm" />
                <source src={ogv} type="video/ogg" />
                Your browser does not support the video tag.
            </video>
        );
    }
}

export default BackgroundVideo;
