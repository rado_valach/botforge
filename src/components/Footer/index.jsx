import React, { Component, PropTypes } from 'react';

import footerCornerImage from './images/footer-corner.png';

class Footer extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer className="nk-footer nk-footer-parallax nk-footer-parallax-opacity">
	            <img className="nk-footer-top-corner" src={footerCornerImage} alt=""/>

	            <div className="container">
	                <div className="nk-gap-2"></div>
	               
	                <p>
	                    &copy; 2017 made with magitech by Thyr
	                </p>

	                <div className="nk-gap-4"></div>
	            </div>
	        </footer>
        );
    }
}

export default Footer;
