const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  inject: 'body'
})

// create CSS file from SASS
const extractSass = new ExtractTextPlugin({
  filename: "stylesheet-[hash].css",
  disable: process.env.NODE_ENV === "development"
});

// clear build folder before every build
const clearBuildFolder = new CleanWebpackPlugin(['build'], {
  root: path.resolve(__dirname , '.'),
  verbose: true,
  dry: false
});

const dashboard = new DashboardPlugin();

module.exports = {
  entry: './src/index.jsx',
  output: {
    path: path.resolve('build'),
    publicPath: path.build,
    filename: 'bundle-[hash].js'
  },
  module: {
    rules: [
      { 
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query:
          {
            presets:['react']
          }
      },
      {
        test: /\.jsx$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query:
          {
            presets:['react']
          }
      },
      {
        test: /\.(scss|css)$/,
        use: extractSass.extract({
            use: [{
                loader: "css-loader",
                options: {
                  minimize: true
                }
            }, {
                loader: "postcss-loader"
            }, {
                loader: "sass-loader"
            }],
            // use style-loader in development
            fallback: "style-loader"
        })
      },
      {
        test: /\.(jp?g|png|svg)$/,
        loader: 'file-loader',
       // loader: "url-loader",
        options: {
          name: '[hash][name].[ext]',
          outputPath: 'assets/images/',
          publicPath: path.build
        }
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: '[hash][name].[ext]',
          outputPath: '/assets/fonts/'
        }
      },
      {
        test: /\.(mp4|ogv|webm)$/,
        loader: 'file-loader',
        options: {
          name: '[hash][name].[ext]',
          outputPath: '/assets/video/'
        }
      },
      {
        test: /\.htaccess$/,
        loader: 'file-loader',
        options: {
          name: '.htaccess',
          outputPath: '/'
        }
      }
    ]
  },
  plugins: [
    clearBuildFolder,
    HtmlWebpackPluginConfig,
    extractSass,
    dashboard
  ]
}