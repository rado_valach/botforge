# Botforge

Botforge is web console used to create and manage Discord "bots" for role-play events.

#### Install modules

```sh
$ yarn install
```

#### Run in local dev-server localhost:8080
This also watches for any changes and makes live realoads
```sh
$ yarn start
```

#### Build
```sh
$ yarn webpack
```